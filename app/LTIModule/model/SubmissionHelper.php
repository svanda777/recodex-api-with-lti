<?php


namespace App\LTIModule\Model;


use App\Exceptions\BadRequestException;
use App\Exceptions\CannotReceiveUploadedFileException;
use App\Exceptions\ForbiddenRequestException;
use App\Exceptions\InvalidArgumentException;
use App\Exceptions\SubmissionFailedException;
use App\Helpers\ExerciseConfig\Compilation\CompilationParams;
use App\Helpers\FailureHelper;
use App\Helpers\JobConfig\Generator as JobConfigGenerator;
use App\Helpers\MonitorConfig;
use App\Helpers\SubmissionHelper as SubmitHelper;
use App\Helpers\UploadedFileStorage;
use App\Model\Entity\Assignment;
use App\Model\Entity\AssignmentSolution;
use App\Model\Entity\AssignmentSolutionSubmission;
use App\Model\Entity\Solution;
use App\Model\Entity\SolutionFile;
use App\Model\Entity\SubmissionFailure;
use App\Model\Entity\User;
use App\Model\Repository\Assignments;
use App\Model\Repository\AssignmentSolutions;
use App\Model\Repository\AssignmentSolutionSubmissions;
use App\Model\Repository\RuntimeEnvironments;
use App\Model\Repository\Solutions;
use App\Model\Repository\SubmissionFailures;
use App\Model\Repository\UploadedFiles;
use App\Model\View\AssignmentSolutionViewFactory;
use App\Security\ACL\IAssignmentPermissions;
use App\V1Module\Presenters\SubmitPresenter;
use Exception;
use Nette\Http\IResponse;
use Nette\Object;

/**
 * Class SubmissionHelper
 * @package App\LTIModule\Model
 */
class SubmissionHelper extends Object {

	/**
	 * @var UploadedFileStorage
	 */
	public $fileStorage;

	/**
	 * @var UploadedFiles
	 */
	public $uploadedFiles;

	/** @var SubmitPresenter */
	public $submitPresenter;

	/** @var IAssignmentPermissions */
	public $assignmentAcl;

	/** @var Assignments */
	public $assignments;

	/** @var RuntimeEnvironments */
	public $runtimeEnvironments;

	/** @var AssignmentSolutions */
	public $assignmentSubmissions;

	/** @var Solutions */
	public $solutions;

	/**
	 * @var SubmissionFailures
	 */
	public $submissionFailures;

	/**
	 * @var JobConfigGenerator
	 */
	public $jobConfigGenerator;

	/**
	 * @var FailureHelper
	 */
	public $failureHelper;

	/**
	 * @var AssignmentSolutionViewFactory
	 */
	public $assignmentSolutionViewFactory;

	/**
	 * @var MonitorConfig
	 */
	public $monitorConfig;

	/**
	 * @var SubmitHelper
	 */
	public $submissionHelper;

	/**
	 * @var AssignmentSolutions
	 */
	public $assignmentSolutions;

	/**
	 * SubmissionHelper constructor.
	 *
	 * @param UploadedFileStorage           $file_storage
	 * @param UploadedFiles                 $uploaded_files
	 * @param SubmitPresenter               $submit_presenter
	 * @param IAssignmentPermissions        $assignment_permissions
	 * @param Assignments                   $assignments
	 * @param RuntimeEnvironments           $runtime_environments
	 * @param AssignmentSolutionSubmissions $assignment_solution_submissions
	 * @param Solutions                     $solutions
	 * @param SubmissionFailures            $submission_failures
	 * @param JobConfigGenerator            $generator
	 * @param FailureHelper                 $failure_helper
	 * @param AssignmentSolutionViewFactory $assignment_solution_view_factory
	 * @param MonitorConfig                 $monitor_config
	 * @param SubmitHelper                  $submission_helper
	 * @param AssignmentSolutions           $assignment_solutions
	 */
	public function __construct(
		UploadedFileStorage $file_storage,
		UploadedFiles $uploaded_files,
		SubmitPresenter $submit_presenter,
		IAssignmentPermissions $assignment_permissions,
		Assignments $assignments,
		RuntimeEnvironments $runtime_environments,
		AssignmentSolutionSubmissions $assignment_solution_submissions,
		Solutions $solutions,
		SubmissionFailures $submission_failures,
		JobConfigGenerator $generator,
		FailureHelper $failure_helper,
		AssignmentSolutionViewFactory $assignment_solution_view_factory,
		MonitorConfig $monitor_config,
		SubmitHelper $submission_helper,
		AssignmentSolutions $assignment_solutions
	) {
		$this->fileStorage                   = $file_storage;
		$this->uploadedFiles                 = $uploaded_files;
		$this->submitPresenter               = $submit_presenter;
		$this->assignmentAcl                 = $assignment_permissions;
		$this->assignments                   = $assignments;
		$this->runtimeEnvironments           = $runtime_environments;
		$this->assignmentSubmissions         = $assignment_solution_submissions;
		$this->solutions                     = $solutions;
		$this->submissionFailures            = $submission_failures;
		$this->jobConfigGenerator            = $generator;
		$this->failureHelper                 = $failure_helper;
		$this->assignmentSolutionViewFactory = $assignment_solution_view_factory;
		$this->monitorConfig                 = $monitor_config;
		$this->submissionHelper              = $submission_helper;
		$this->assignmentSolutions           = $assignment_solutions;
	}

	/**
	 * Handler for upload submission files.
	 *
	 * @param $user
	 * @param $files
	 *
	 * @return \App\Model\Entity\UploadedFile|NULL
	 * @throws BadRequestException
	 * @throws CannotReceiveUploadedFileException
	 * @throws InvalidArgumentException
	 */
	public function uploadFile(
		User $user,
		UploadedFiles $files
	) {
		if ( count( $files ) === 0 ) {
			throw new BadRequestException( "No file was uploaded" );
		} elseif ( count( $files ) > 1 ) {
			throw new BadRequestException( "Too many files were uploaded" );
		}

		$file         = array_pop( $files );
		$uploadedFile = $this->fileStorage->store( $file, $user );

		if ( $uploadedFile === null ) {
			throw new CannotReceiveUploadedFileException( $file->getName() );
		}

		$this->uploadedFiles->persist( $uploadedFile );
		$this->uploadedFiles->flush();

		return $uploadedFile;
	}

	/**
	 * Add uploaded file to submission and send submission.
	 *
	 * @param $assignment
	 * @param $user
	 * @param $uploadedFile
	 * @param $note
	 *
	 * @throws ForbiddenRequestException
	 * @throws InvalidArgumentException
	 * @throws \App\Exceptions\NotFoundException
	 * @throws \App\Exceptions\SubmissionFailedException
	 */
	public function submitSolution(
		$assignment,
		$user,
		$uploadedFile,
		$note
	) {
		$assignment = $this->assignments->findOrThrow( $assignment );
		if ( ! $this->assignmentAcl->canSubmit( $assignment ) ) {
			throw new ForbiddenRequestException();
		}

		if ( ! $this->canReceiveSubmissions( $assignment, $user ) ) {
			throw new ForbiddenRequestException( "User '{$user->getId()}' cannot submit solutions for this assignment anymore." );
		}

		// retrieve and check uploaded files
		$uploadedFiles = $this->uploadedFiles->findAllById( $uploadedFile->getId() );
		if ( count( $uploadedFiles ) === 0 ) {
			throw new InvalidArgumentException( "files", "No files were uploaded" );
		}


		$runtimeEnvironment = $this->runtimeEnvironments->detectOrThrow( $assignment, $uploadedFiles );


		// create Solution object
		$solution = new Solution( $user, $runtimeEnvironment );

		$submittedFiles = [];
		foreach ( $uploadedFiles as $file ) {
			if ( $file instanceof SolutionFile ) {
				throw new ForbiddenRequestException( "File {$file->getId()} was already used in a different submission." );
			}

			$submittedFiles[] = $file->getName();
			$solutionFile     = SolutionFile::fromUploadedFile( $file, $solution );
			$this->uploadedFiles->persist( $solutionFile, false );
			$this->uploadedFiles->remove( $file, false );
		}

		// create and fill assignment solution
		$assignmentSolution = AssignmentSolution::createSolution( $note, $assignment, $solution );

		// persist all changes and send response
		$this->assignmentSolutions->persist( $assignmentSolution );
		$this->solutions->persist( $solution );
		$this->finishSubmission( $assignmentSolution, false, $user );
	}

	/**
	 * Handler submission failed.
	 *
	 * @param AssignmentSolutionSubmission $submission
	 * @param string                       $message
	 *
	 * @throws SubmissionFailedException
	 */
	private function submissionFailed( AssignmentSolutionSubmission $submission, string $message ) {
		$failure = SubmissionFailure::forSubmission( SubmissionFailure::TYPE_BROKER_REJECT, $message, $submission );
		$this->submissionFailures->persist( $failure );
		$this->failureHelper->report( FailureHelper::TYPE_BACKEND_ERROR,
			"Failed to send submission {$submission->getId()} to the broker" );
		throw new SubmissionFailedException( $message );
	}

	/**
	 * Take a complete submission entity and submit it to the backend
	 *
	 * @param AssignmentSolution $solution a persisted submission entity
	 * @param bool               $isDebug
	 *
	 * @return array The response that can be sent to the client
	 * @throws ForbiddenRequestException
	 * @throws InvalidArgumentException
	 * @throws SubmissionFailedException
	 */
	private function finishSubmission( AssignmentSolution $solution, bool $isDebug = false, User $user ) {
		if ( $solution->getId() === null ) {
			throw new InvalidArgumentException( "The submission object is missing an id" );
		}

		// check for the license of instance of user
		$assignment = $solution->getAssignment();
		if ( $assignment->getGroup()->hasValidLicence() === false ) {
			throw new ForbiddenRequestException( "Your institution '{$assignment->getGroup()->getInstance()->getId()}' does not have a valid licence and you cannot submit solutions for any assignment in this group '{$assignment->getGroup()->getId()}'. Contact your supervisor for assistance.",
				IResponse::S402_PAYMENT_REQUIRED );
		}

		// generate job configuration
		$compilationParams = CompilationParams::create( $solution->getSolution()->getFileNames(), $isDebug );
		$generatorResult   =
			$this->jobConfigGenerator->generateJobConfig( $user,
				$solution->getAssignment(),
				$solution->getSolution()->getRuntimeEnvironment(),
				$compilationParams );

		// create submission entity
		$submission = new AssignmentSolutionSubmission( $solution,
			$generatorResult->getJobConfigPath(), $user );
		$this->assignmentSubmissions->persist( $submission );

		// initiate submission
		$resultsUrl = null;
		try {
			$resultsUrl = $this->submissionHelper->submit(
				$submission->getId(),
				$solution->getSolution()->getRuntimeEnvironment()->getId(),
				$solution->getSolution()->getFiles()->getValues(),
				$generatorResult->getJobConfig()
			);
		}
		catch ( Exception $e ) {
			$this->submissionFailed( $submission, $e->getMessage() );
		}

		// If the submission was accepted we now have the URL where to look for the results later -> persist it
		$submission->setResultsUrl( $resultsUrl );
		$this->assignmentSubmissions->persist( $submission );

		return [
			"submission"       => $this->assignmentSolutionViewFactory->getSolutionData( $solution ),
			"webSocketChannel" => [
				"id"                 => $generatorResult->getJobConfig()->getJobId(),
				"monitorUrl"         => $this->monitorConfig->getAddress(),
				"expectedTasksCount" => $generatorResult->getJobConfig()->getTasksCount()
			]
		];
	}

	/**
	 * Determine if given user can submit solutions to assignment.
	 *
	 * @param Assignment $assignment
	 * @param User|NULL  $user
	 *
	 * @return bool
	 */
	private function canReceiveSubmissions( Assignment $assignment, User $user = null ) {
		return $assignment->isPublic() &&
		       $assignment->getGroup()->hasValidLicence() &&
		       ( $user !== null &&
		         count( $this->assignmentSolutions->findValidSolutions( $assignment, $user ) )
		         <= $assignment->getSubmissionsCountLimit() );
	}
}