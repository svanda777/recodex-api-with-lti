<?php

namespace App\LTIModule\Model;

use PDO;
use PDOException;

/**
 * Class DatabaseHelper
 *
 * Class which support work with database
 *
 * @package App\LTIModule\Model
 */
class DatabaseHelper {

	/**
	 * @param      $db
	 * @param null $checkSession
	 *
	 * @return bool connection checker
	 */
	public function init( &$db, $checkSession = null ) {
		$ok = true;

		/** Checks sessions */
		if ( ! is_null( $checkSession ) && $checkSession ) {
			$ok = isset( $_SESSION['consumer_pk'] ) && ( isset( $_SESSION['resource_pk'] ) || is_null( $_SESSION['resource_pk'] ) ) &&
			      isset( $_SESSION['user_consumer_pk'] ) && ( isset( $_SESSION['user_pk'] ) || is_null( $_SESSION['user_pk'] ) ) && isset( $_SESSION['isStudent'] );
		}
		if ( ! $ok ) {
			$_SESSION['error_message'] = 'Unable to open session.';
		} else {

			/** Checks database */
			$db = $this->open_db();
			$ok = $db !== false;
			if ( ! $ok ) {
				if ( ! is_null( $checkSession ) && $checkSession ) {
					$_SESSION['error_message'] = 'Unable to open database.';
				}
			}
		}

		return $ok;
	}


	/**
	 * Connect to database via PDO.
	 *
	 * @return bool|PDO database
	 */
	public function open_db() {
		try {
			$db = new PDO( 'mysql:dbname=recodex;host=localhost', 'recodex', 'recodex' );
			/** TODO: Gets database credentials */
		}
		catch ( PDOException $e ) {
			$db                        = false;
			$_SESSION['error_message'] = "Database error {$e->getCode()}: {$e->getMessage()}";
		}

		return $db;
	}
}