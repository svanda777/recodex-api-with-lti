<?php


namespace App\LTIModule\Model;


use IMSGlobal\LTI\Profile;
use IMSGlobal\LTI\ToolProvider;
use Tracy\Debugger;


/**
 * Class ReCodExToolProvider
 * @package App\LTIModule\Model
 */
class ReCodExToolProvider extends ToolProvider\ToolProvider {

	/**
	 * ReCodExToolProvider constructor.
	 *
	 * Prepared basic information about tool provider.
	 *
	 * @param $data_connector
	 */
	function __construct( $data_connector ) {
		parent::__construct( $data_connector );

		$this->baseUrl = 'http://localhost:4000/lti/';

		/** Author info */
		$this->vendor = new Profile\Item( 'recodex-team',
			'ReCodEx Team',
			'Jan Buchar, Martin Kruliš, Martin Polanka, Šimon Rozšíval, Petr Stefan',
			'http://www.ksi.mff.cuni.cz/cs/sw/recodex.html' );

		/** System info */
		$this->product = new Profile\Item( 'recodex',
			'ReCodEx',
			'ReCodEx is a system for dynamic analysis and evaluation of programming exercises.',
			'https://github.com/ReCodEx',
			'1.0.00' );

		$requiredMessages = array(
			new Profile\Message( 'basic-lti-launch-request',
				'launch',
				array(
					'User.id',
					'Membership.role',
					'Person.name.family',
					'Person.name.given',
					'Person.email.primary'
				) )
		);

		$optionalMessages = array(
			new Profile\Message( 'ContentItemSelectionRequest', 'contentItem', array( 'Membership.role' ) ),
		);

		$this->resourceHandlers[] = new Profile\ResourceHandler(
			new Profile\Item( 'recodex-api',
				'ReCodEx',
				'ReCodEx is a system for dynamic analysis and evaluation of programming exercises.' ), '../logo.png',
			$requiredMessages, $optionalMessages );

		$this->requiredServices[] = new Profile\ServiceDefinition(
			array( 'application/vnd.ims.lti.v2.toolproxy+json' ),
			array( 'POST' ) );

	}

	/**
	 * Display assignment detail.
	 *
	 * @return bool|void
	 */
	function onLaunch() {
		$_SESSION['return_url'] = $this->returnUrl;
	}

	/**
	 * User can choose right assignment
	 *
	 * @return bool|void
	 */
	function onContentItem() {
		$_SESSION['consumer_pk'] = $this->consumer->getRecordId();
		$_SESSION['return_url']  = $this->returnUrl;
		$_SESSION['lti_version'] = $_POST['lti_version'];
	}

	/**
	 * Registration handler
	 *
	 * @return bool|void redirection url
	 */
	function onRegister() {
		$_SESSION['consumer_pk']    = $this->consumer->getRecordId();
		$_SESSION['tc_profile_url'] = $_POST['tc_profile_url'];
		$_SESSION['tc_profile']     = $this->consumer->profile;
		$_SESSION['return_url']     = $_POST['launch_presentation_return_url'];

		$this->redirectUrl = $this->baseUrl . 'install/confirmation';
	}

	/**
	 * Error handler
	 *
	 * Write error and log it.
	 *
	 * @return bool|void error message
	 */
	function onError() {
		$msg = $this->message;
		if ( $this->debugMode && ! empty( $this->reason ) ) {
			$msg = $this->reason;
			Debugger::log( $msg, Debugger::ERROR );
		}
		$this->errorOutput = $msg;
	}

	/**
	 * Return Tool consumer id.
	 *
	 * @return int consumer id
	 */
	public function getToolConsumer() {
		return $this->consumer ? $this->consumer->getRecordId() : $_SESSION['consumer_pk'];
	}

	/**
	 * Return tool consumer return url.
	 *
	 * @return string return url
	 */
	public function getReturnUrl() {
		return $this->returnUrl ?? $_SESSION['return_url'];
	}

	/**
	 *  Return used version of LTI.
	 *
	 * @return string lti version
	 */
	public function getReturnVersion() {
		return $_SESSION['lti_version'];
	}
}