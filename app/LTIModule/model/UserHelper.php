<?php


namespace App\LTIModule\Model;


use App\Helpers\EmailVerificationHelper;
use App\Helpers\ExternalLogin\ExternalServiceAuthenticator;
use App\Model\Entity\Instance;
use App\Model\Repository\Groups;
use App\Model\Repository\Instances;
use App\Model\Repository\Users;
use Nelmio\Alice\support\models\Group;


/**
 * Class UserHelper
 *
 * Authenticate user via external login service.
 *
 * @package App\LTIModule\Model
 */
class UserHelper {

	/**
	 * @var ExternalServiceAuthenticator
	 */
	public $externalServiceAuthenticator;

	/**
	 * @var Users
	 */
	public $users;

	/**
	 * @var Groups
	 */
	public $groups;

	/**
	 * @var Instances
	 */
	public $instances;

	/**
	 * @var EmailVerificationHelper
	 */
	public $emailVerificationHelper;

	/** @var Instance */
	protected $instance;

	/**
	 * UserHelper constructor.
	 *
	 * @param ExternalServiceAuthenticator $external_service_authenticator
	 * @param Users                        $users
	 * @param Groups                       $groups
	 * @param Instances                    $instances
	 * @param EmailVerificationHelper      $email_verification_helper
	 * @param string                       $instance
	 */
	public function __construct(
		ExternalServiceAuthenticator $external_service_authenticator,
		Users $users,
		Groups $groups,
		Instances $instances,
		EmailVerificationHelper $email_verification_helper,
		string $instance
	) {
		$this->externalServiceAuthenticator = $external_service_authenticator;
		$this->users                        = $users;
		$this->groups                       = $groups;
		$this->instances                    = $instances;
		$this->emailVerificationHelper      = $email_verification_helper;
		$this->instance                     = $instance;
	}


	/**
	 * Authenticate user with taken credentials. Can register user.
	 *
	 * @param $credentials array credentials
	 * @param $group       Group assignment group
	 *
	 * @return \App\Model\Entity\User|null
	 * @throws \App\Exceptions\BadRequestException
	 * @throws \App\Exceptions\InvalidStateException
	 * @throws \App\Exceptions\WrongCredentialsException
	 */
	public function authenticate( $credentials, $group ) {
		$service = $this->externalServiceAuthenticator->findService( 'lti', 'lti2' );
		$user = $this->users->getByEmail( $credentials['email'] );
		$instance = $this->instances->findOrThrow( $this->instance );
		$group = $this->groups->findOrThrow( $group->id );
		if ( $user ) {
			$user = $this->externalServiceAuthenticator->authenticate( $service, $credentials );
		} else {
			$user = $this->externalServiceAuthenticator->register( $service, $instance, $credentials );
			if ( ! $user->isVerified() ) {
				$this->emailVerificationHelper->process( $user );
			}
		}
		if ( $group->isStudentOf( $user ) === false ) {
			$user->makeStudentOf( $group );
			$this->groups->flush();
		}

		return $user;
	}
}