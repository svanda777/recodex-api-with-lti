<?php


namespace App\LTIModule\Model;


use App\Helpers\ExternalLogin\IExternalLoginService;
use App\Helpers\ExternalLogin\UserData;

class LTILoginService implements IExternalLoginService {

	/**
	 * Gets identifier for this service
	 * @return string Login service unique identifier
	 */
	function getServiceId(): string {
		return 'lti';
	}

	/**
	 * Gets the identifier of the type of authentication of the service.
	 * @return string Login service unique identifier
	 */
	function getType(): string {
		return 'lti2';
	}

	/**
	 * Read user's data from the identity provider, if the credentials provided by the user are correct.
	 *
	 * @param array $credentials credentials
	 *
	 * @return UserData Information known about this user
	 */
	function getUser( $credentials ): ?UserData {
		return new UserData( $credentials['userID'],
			array( $credentials['email'] ),
			$credentials['firstName'],
			$credentials['lastName'],
			"",
			"" );
	}
}