<?php


namespace App\LTIModule\Presenters;


use App\Exceptions\BadRequestException;
use App\Exceptions\CannotReceiveUploadedFileException;
use App\Exceptions\ForbiddenRequestException;
use App\Exceptions\InvalidAccessTokenException;
use App\Exceptions\InvalidArgumentException;
use App\Exceptions\InvalidStateException;
use App\Exceptions\NotFoundException;
use App\Exceptions\SubmissionFailedException;
use App\Exceptions\WrongCredentialsException;
use App\LTIModule\Model\SubmissionHelper;
use App\LTIModule\Model\UserHelper;
use App\Model\Repository\Assignments;
use App\Security\AccessManager;
use App\Security\AccessToken;
use App\Security\Identity;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;

/**
 * Class LaunchPresenter
 * @package App\LTIModule\Presenters
 */
class LaunchPresenter extends BasePresenter {

	/**
	 * @var Assignments
	 * @inject
	 */
	public $assignments;

	/**
	 * @var UserHelper
	 * @inject
	 */
	public $userHelper;

	/**
	 * @var AccessManager
	 * @inject
	 */
	public $accessManager;

	/**
	 * @var SubmissionHelper
	 * @inject
	 */
	public $submissionHelper;

	/**
	 * Render assignment detail.
	 */
	public function renderDefault() {
		$req        = $this->getRequest();
		$assignment = $this->assignments->findOrThrow( $req->getPost( 'custom_assignment' ) );
		$this->logIn( $assignment );
		$this->template->assignment          = $assignment;
		$this->template->assignmentLocalized = $assignment->localizedTexts->get( self::PREFERRED_LANGUAGE );
	}

	/**
	 * Create component for submit solution.
	 *
	 * @return Form
	 */
	protected function createComponentSubmitSolution() {
		$form = new Form();
		$form->addHidden( 'user_id', $this->request->post['user_id'] );
		$form->addHidden( 'lis_person_contact_email_primary',
			$this->request->post['lis_person_contact_email_primary'] );
		$form->addHidden( 'lis_person_name_given', $this->request->post['lis_person_name_given'] );
		$form->addHidden( 'lis_person_name_family', $this->request->post['lis_person_name_family'] );
		$form->addHidden( 'custom_assignment', $this->request->post['custom_assignment'] );
		$form->addUpload( 'solution', 'Source code' )->setRequired();
		$form->addText( 'note', 'Comment for teacher' );
		$form->addSubmit( 'submitSolution', 'Submit solution' );
		$form->onSuccess[] = [ $this, 'submitSolutionSucceeded' ];

		return $form;
	}

	/**
	 * Submit solution.
	 *
	 * @param       $form
	 * @param array $values
	 */
	public function submitSolutionSucceeded( $form, array $values ) {
		$url = $_SESSION['return_url'];
		$sep = strpos( $url, '?' ) === false ? '?' : '&';
		try {
			$assignmentId = $values['custom_assignment'];
			$note         = $values['note'];
			$assignment   = $this->assignments->findOrThrow( $assignmentId );
			$this->logIn( $assignment );
			$req = $this->getRequest();
			/** @var Identity $identity */
			$identity = $this->getUser()->getIdentity();

			if ( $identity === null || $identity->getUserData() === null ) {
				throw new ForbiddenRequestException();
			}

			$user         = $identity->getUserData();
			$uploadedFile = $this->submissionHelper->uploadFile( $user, $req->getFiles() );
			$this->submissionHelper->submitSolution( $assignmentId, $user, $uploadedFile, $note );
			header( "Location: {$url}{$sep}lti_msg=Uploaded&status=success" );
			exit;
		}
		catch ( ForbiddenRequestException $e ) {
			header( "Location: {$url}{$sep}lti_msg=ForbiddenRequestException&status=failure" );
			exit;
		}
		catch ( InvalidArgumentException $e ) {
			header( "Location: {$url}{$sep}lti_msg=InvalidArgumentException&status=failure" );
			exit;
		}
		catch ( NotFoundException $e ) {
			header( "Location: {$url}{$sep}lti_msg=NotFoundException&status=failure" );
			exit;
		}
		catch ( SubmissionFailedException $e ) {
			header( "Location: {$url}{$sep}lti_msg=SubmissionFailedException&status=failure" );
			exit;
		}
		catch ( BadRequestException $e ) {
			header( "Location: {$url}{$sep}lti_msg=BadRequestException&status=failure" );
			exit;
		}
		catch ( CannotReceiveUploadedFileException $e ) {
			header( "Location: {$url}{$sep}lti_msg=CannotReceiveUploadedFileException&status=failure" );
			exit;
		}
	}


	/**
	 * Login or register user.
	 *
	 * @param $assignment
	 */
	protected function logIn( $assignment ) {
		try {
			$req         = $this->getRequest();
			$credentials = array(
				'email'     => $req->getPost( 'lis_person_contact_email_primary' ),
				'userID'    => $req->getPost( 'user_id' ),
				'firstName' => $req->getPost( 'lis_person_name_given' ),
				'lastName'  => $req->getPost( 'lis_person_name_family' )
			);
			$user        = $this->userHelper->authenticate( $credentials, $assignment->group );
			$token       = $this->accessManager->issueToken( $user, [ AccessToken::SCOPE_REFRESH ] );
			$this->getUser()->login( new Identity( $user, $this->accessManager->decodeToken( $token ) ) );
		}
		catch ( BadRequestException $e ) {
			$url = $_SESSION['return_url'];
			$sep = strpos( $url, '?' ) === false ? '?' : '&';
			header( "Location: {$url}{$sep}lti_msg=BadRequestException&status=failure" );
			exit;
		}
		catch ( ForbiddenRequestException $e ) {
			$url = $_SESSION['return_url'];
			$sep = strpos( $url, '?' ) === false ? '?' : '&';
			header( "Location: {$url}{$sep}lti_msg=ForbiddenRequestException&status=failure" );
			exit;
		}
		catch ( InvalidAccessTokenException $e ) {
			$url = $_SESSION['return_url'];
			$sep = strpos( $url, '?' ) === false ? '?' : '&';
			header( "Location: {$url}{$sep}lti_msg=InvalidAccessTokenException&status=failure" );
			exit;
		}
		catch ( InvalidStateException $e ) {
			$url = $_SESSION['return_url'];
			$sep = strpos( $url, '?' ) === false ? '?' : '&';
			header( "Location: {$url}{$sep}lti_msg=InvalidStateException&status=failure" );
			exit;
		}
		catch ( WrongCredentialsException $e ) {
			$url = $_SESSION['return_url'];
			$sep = strpos( $url, '?' ) === false ? '?' : '&';
			header( "Location: {$url}{$sep}lti_msg=WrongCredentialsException&status=failure" );
			exit;
		}
		catch ( AuthenticationException $e ) {
			$url = $_SESSION['return_url'];
			$sep = strpos( $url, '?' ) === false ? '?' : '&';
			header( "Location: {$url}{$sep}lti_msg=AuthenticationException&status=failure" );
			exit;
		}

	}
}