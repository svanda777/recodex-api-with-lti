<?php


namespace App\LTIModule\Presenters;


use IMSGlobal\LTI\ToolProvider\ToolConsumer;
use Nette\Application\UI\Form;

/**
 * Class InstallPresenter
 *
 * Creates LTI v2 connection between TP and TC.
 *
 * @package App\LTIModule\Presenters
 */
class InstallPresenter extends BasePresenter {


	/**
	 * First communication between tool provider and consumer.
	 */
	public function actionDefault() {

	}

	/**
	 * Confirmation form for user.
	 *
	 * @return Form
	 */
	protected function createComponentConfirmationForm() {
		$form = new Form();
		$form->addSubmit( 'confirm', 'Register' );
		$form->addSubmit( 'cancel', 'Cancel' )->setValidationScope( false )->onClick[] = [
			$this,
			'confirmationFormCancel'
		];
		$form->onSuccess[]                                                             = [
			$this,
			'confirmationFormOnSucceeded'
		];

		return $form;
	}

	/**
	 * Send cancel request back to tool consumer.
	 */
	public function confirmationFormCancel() {
		$url = $_SESSION['return_url'];
		$sep = strpos( $url, '?' ) === false ? '?' : '&';
		header( "Location: {$url}{$sep}lti_msg=The%20tool%20registration%20has%20been%20cancelled&status=failure" );
		exit;
	}

	/**
	 * Confirm cooperation between TP and TC.
	 */
	public function confirmationFormOnSucceeded() {
		$url                          = $_SESSION['return_url'];
		$sep                          = strpos( $url, '?' ) === false ? '?' : '&';
		$this->toolProvider->consumer = ToolConsumer::fromRecordId( $_SESSION['consumer_pk'], $this->dataConnector );
		$ok                           = $this->toolProvider->doToolProxyService( $_SESSION['tc_profile_url'] );
		if ( $ok ) {
			$guid = $this->toolProvider->consumer->getKey();
			header( "Location: {$url}{$sep}lti_msg=The%20tool%20has%20been%20registered&status=success&tool_proxy_guid={$guid}" );
			exit;
		} else {
			header( "Location: {$url}{$sep}lti_msg=Error%20setting%20tool%20proxy&status=failure" );
			exit;
		}
	}
}