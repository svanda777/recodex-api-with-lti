<?php


namespace App\LTIModule\Presenters;


use App\Model\Repository\Groups;
use IMSGlobal\LTI\ToolProvider\ContentItem;
use IMSGlobal\LTI\ToolProvider\ToolProvider;

/**
 * Class ContentItemPresenter
 *
 * Function replacing onContentItem of Tool Provider
 *
 * @package App\LTIModule\Presenters
 */
class ContentItemPresenter extends BasePresenter {

	/**
	 * @var Groups
	 * @inject
	 */
	public $groups;

	/**
	 * Render list of available assignments.
	 */
	public function renderDefault() {
		$external_id                 = $this->request->getPost( 'lis_course_section_sourcedid' );
		$group                       = $this->groups->getByExternalID( $external_id );
		$this->template->assignments = $group->getAssignments();
		$this->template->languages   = [ self::PREFERRED_LANGUAGE, self::SECONDARY_LANGUAGE ];
	}

	/**
	 * Send chosen assignment back to tool consumer.
	 *
	 * @param $id   int assignment id
	 * @param $name string assignment name
	 */
	public function actionChooseAssignment( $id, $name ) {
		$req  = $this->getRequest();
		$item = new ContentItem( 'LtiLinkItem' );
		$item->setMediaType( ContentItem::LTI_LINK_MEDIA_TYPE );
		$item->setTitle( $name );
		$item->custom                 = array( 'assignment' => $id );
		$form_params['content_items'] = ContentItem::toJson( $item );
		$form_params                  = $this->toolConsumer->signParameters( $this->returnUrl,
			'ContentItemSelection',
			$this->ltiVersion,
			$form_params );
		$page                         = ToolProvider::sendForm( $this->returnUrl, $form_params );
		echo $page;
		exit;
	}
}