<?php


namespace App\LTIModule\Presenters;


use App\LTIModule\Model\DatabaseHelper;
use App\LTIModule\Model\ReCodExToolProvider;
use IMSGlobal\LTI\ToolProvider\DataConnector\DataConnector;
use IMSGlobal\LTI\ToolProvider\ToolConsumer;
use IMSGlobal\LTI\ToolProvider\ToolProvider;
use Nette\Application\UI\Presenter;
use Parsedown;

/**
 * Class BasePresenter
 * @package App\LTIModule\Presenters
 */
abstract class BasePresenter extends Presenter {

	/**
	 * @var ToolProvider
	 */
	protected $toolProvider;

	/**
	 * @var DataConnector
	 */
	protected $dataConnector;


	/**
	 * @var ToolConsumer
	 */
	protected $toolConsumer;

	/**
	 * Tool consumer return url
	 */
	protected $returnUrl;

	/**
	 * Tool consumer lti version
	 */
	protected $ltiVersion;

	/**
	 * @var DatabaseHelper
	 * @inject
	 */
	public $databaseHelper;

	/**
	 * Language for localized texts;
	 */
	protected const PREFERRED_LANGUAGE = "cs";

	/**
	 * Secondary Language for localized texts;
	 */
	protected const SECONDARY_LANGUAGE = "en";

	/**
	 * Create tool provider.
	 */
	public function startup() {
		parent::startup();
		if ( $this->databaseHelper->init( $db ) ) {
			$this->dataConnector = DataConnector::getDataConnector( '', $db );
			$this->toolProvider  = new ReCodExToolProvider( $this->dataConnector );
			$this->toolProvider->setParameterConstraint( 'oauth_consumer_key',
				true,
				50,
				array( 'basic-lti-launch-request', 'ContentItemSelectionRequest' ) );
			$this->toolProvider->setParameterConstraint( 'resource_link_id',
				true,
				50,
				array( 'basic-lti-launch-request' ) );
			$this->toolProvider->setParameterConstraint( 'user_id', true, 50, array( 'basic-lti-launch-request' ) );
			$this->toolProvider->setParameterConstraint( 'roles', true, null, array( 'basic-lti-launch-request' ) );
		} else {
			$this->toolProvider         = new ReCodExToolProvider( null );
			$this->toolProvider->reason = $_SESSION['error_message'];
		}
		$this->toolProvider->handleRequest();
		$this->toolConsumer = ToolConsumer::fromRecordId( $this->toolProvider->getToolConsumer(),
			$this->dataConnector );
		$this->returnUrl    = $this->toolProvider->getReturnUrl();
		$this->ltiVersion   = $this->toolProvider->getReturnVersion();
	}

	/**
	 * Add filters to templates.
	 */
	public function beforeRender() {
		parent::beforeRender();
		$this->template->addFilter( 'markdown',
			function ( $s ) {
				$Parsedown = new Parsedown();

				return $Parsedown->text( $s );
			} );
		$this->template->addFilter( 'booleanify',
			function ( $s ) {
				return $s ? 'true' : 'false';
			} );
	}
}