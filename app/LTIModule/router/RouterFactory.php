<?php

namespace App\LTIModule;

use Nette;
use Nette\Application\IRouter;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


/**
 * Router factory for LTI module.
 */
class RouterFactory {

	use Nette\StaticClass;

	/**
	 * Create router with all routes for LTI module.
	 * @return IRouter
	 */
	public static function createRouter() {
		$router   = new RouteList( "LTI" );
		$prefix   = "lti/";
		$router[] = new Route( $prefix . "install[/<action>]", "Install:default" );
		$router[] = new Route( $prefix . "contentItem[/<action>]", "ContentItem:default" );
		$router[] = new Route( $prefix . "launch[/<action>]", "Launch:default" );
		return $router;
	}
}
